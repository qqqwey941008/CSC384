from BayesianNetwork import *

##Implement all of the following functions

## Do not modify any of the objects passed in as parameters!
## Create a new Factor object to return when performing factor operations



'''
multiply_factors(factors)

Parameters :
              factors : a list of factors to multiply
Return:
              a new factor that is the product of the factors in "factors"
'''
def _multiply_facotrs(factor1, factor2):
    scope = factor1.get_scope()
    for s in factor2.get_scope():
        if not s in scope:
            scope.append(s)

    new = Factor(factor1.name + ' X ' + factor2.name, scope)

    for assignment in new.get_assignment_iterator():
        f1_val = []
        f2_val = []
        for f1_var in factor1.get_scope():
            f1_val.append(assignment[scope.index(f1_var)])
        for f2_var in factor2.get_scope():
            f2_val.append(assignment[scope.index(f2_var)])
        v = factor1.get_value(f1_val) * factor2.get_value(f2_val)
        new.add_value_at_assignment(v, assignment)
    return new

def multiply_factors(factors):
    for i in range(len(factors) - 1):
        if i == 0:
            new_factor = _multiply_facotrs(factors[i], factors[i+1])
        else:
            new_factor = _multiply_facotrs(new_factor, factors[i+1])
    return new_factor

'''
restrict_factor(factor, variable, value):

Parameters :
              factor : the factor to restrict
              variable : the variable to restrict "factor" on
              value : the value to restrict to
Return:
              A new factor that is the restriction of "factor" by
              "variable"="value"
      
              If "factor" has only one variable its restriction yields a 
              constant factor
'''
def restrict_factor(factor, variable, value):
    scope = factor.get_scope()
    index = scope.index(variable)
    scope.remove(variable)
    new_factor = Factor(factor.name + ', ' + variable.name + ' = ' + str(value), scope)
    for assignment in factor.get_assignment_iterator():
        if assignment[index] == value:
            a = assignment[:]
            a.pop(index)
            new_factor.add_value_at_assignment(factor.get_value(assignment), a)
    return new_factor
    
'''    
sum_out_variable(factor, variable)

Parameters :
              factor : the factor to sum out "variable" on
              variable : the variable to sum out
Return:
              A new factor that is "factor" summed out over "variable"
'''
def sum_out_variable(factor, variable):
    v_val = {}
    scope = factor.get_scope()
    index = scope.index(variable)
    scope.remove(variable)
    new_factor = Factor(factor.name + ' sum out ' + variable.name, scope)

    for assignment in factor.get_assignment_iterator():
        a = assignment[:]
        a.pop(index)
        a_t = tuple(a)
        if not a_t in v_val:
            v_val[a_t] = 0
        v_val[a_t] += factor.get_value(assignment)
  
    for key, val in v_val.items():
        new_factor.add_value_at_assignment(val, list(key))

    return new_factor
    
'''
VariableElimination(net, queryVar, evidenceVars)

 Parameters :
              net: a BayesianNetwork object
              queryVar: a Variable object
                        (the variable whose distribution we want to compute)
              evidenceVars: a list of Variable objects.
                            Each of these variables should have evidence set
                            to a particular value from its domain using
                            the set_evidence function. 

 Return:
         A distribution over the values of QueryVar
 Format:  A list of numbers, one for each value in QueryVar's Domain
         -The distribution should be normalized.
         -The i'th number is the probability that QueryVar is equal to its
          i'th value given the setting of the evidence
 Example:

 QueryVar = A with Dom[A] = ['a', 'b', 'c'], EvidenceVars = [B, C]
 prior function calls: B.set_evidence(1) and C.set_evidence('c')

 VE returns:  a list of three numbers. E.g. [0.5, 0.24, 0.26]

 These numbers would mean that Pr(A='a'|B=1, C='c') = 0.5
                               Pr(A='a'|B=1, C='c') = 0.24
                               Pr(A='a'|B=1, C='c') = 0.26
'''       
def VariableElimination(net, queryVar, evidenceVars):
    factors = net.factors()
    #print(factors[4].print_table())
    #step1
    new_f = []
    for factor in factors:
        for e_var in evidenceVars:
            if e_var in factor.get_scope():
                factor = restrict_factor(factor, e_var, e_var.get_evidence())
                #print(factor)
        if len(factor.get_scope()) > 0:
            new_f.append(factor)
    factors = new_f
    #print(new_f[4].print_table())
    ordering = min_fill_ordering(factors, queryVar)
    #print(ordering)
    #step2
    for var in ordering:
        factors_contain_var = []
        for factor in factors:
            if var in factor.get_scope():
                factors_contain_var.append(factor)
        if len(factors_contain_var) == 0:
            continue
        elif len(factors_contain_var) == 1:
            new = factors_contain_var[0]
        else:
            new = multiply_factors(factors_contain_var)

        new = sum_out_variable(new, var)
        
        for factor in factors_contain_var:
            factors.remove(factor)
        factors.append(new)
        #print(new.print_table())
    
    #step3
    if len(factors) == 1:
        final_factor = factors[0]
    else:
        final_factor = multiply_factors(factors)
    #print(final_factor.print_table())
    output = []
    scope = final_factor.get_scope()
    for assignment in final_factor.get_assignment_iterator():
        output.append(final_factor.get_value(assignment))
    output_sum = sum(output)
    for i in range(len(output)):
        output[i] = output[i] / output_sum
    return output
  
