#Look for #IMPLEMENT tags in this file. These tags indicate what has
#to be implemented to complete the warehouse domain.

'''
rushhour STATESPACE
'''
#   You may add only standard python imports---i.e., ones that are automatically
#   available on CDF.
#   You may not remove any imports.
#   You may not import or otherwise source any of your own files

from search import *
from random import randint

##################################################
# The search space class 'rushhour'             #
# This class is a sub-class of 'StateSpace'      #
##################################################


class rushhour(StateSpace):
    def __init__(self, action, gval, parent, board_properties, vehicle_statuses):
#IMPLEMENT
        """Initialize a rushhour search state object."""
        StateSpace.__init__(self, action, gval, parent)
        self.board_properties = board_properties
        self.vehicle_statuses = vehicle_statuses
        self.occupied()


    def successors(self):
#IMPLEMENT
        '''Return list of rushhour objects that are the successors of the current object'''
        # [<name>, <loc>, <length>, <is_horizontal>, <is_goal>]
        states = []
        (m, n) = self.board_properties[0]
        for i in range(len(self.vehicle_statuses)):
            vehicle = self.vehicle_statuses[i]
            if vehicle[3]:
                for direction in ['W', 'E']:
                    new_loc = ((vehicle[1][0] + (direction == 'E')*vehicle[2]
                                - (direction == 'W')) % n, vehicle[1][1])
                    if self.board[new_loc[0]][new_loc[1]]:
                        continue
                    if direction == 'E':
                        new_loc = ((vehicle[1][0] + (direction == 'E')
                                    - (direction == 'W')) % n, vehicle[1][1])                        
                    new_statuses = self.vehicle_statuses[:]                    
                    new_statuses[i] = self.vehicle_statuses[i][:]
                    new_statuses[i][1] = new_loc
                    states.append(rushhour(direction, self.gval + 1, self, self.board_properties, new_statuses))
            else:
                for direction in ['N', 'S']:
                    new_loc = (vehicle[1][0], (vehicle[1][1] + (direction == 'S')*vehicle[2] - (direction == 'N')) % m)
                    if self.board[new_loc[0]][new_loc[1]]:
                        continue
                    if direction == 'S':
                        new_loc = (vehicle[1][0], (vehicle[1][1] + (direction == 'S') - (direction == 'N')) % m)
                    new_statuses = self.vehicle_statuses[:]  
                    new_statuses[i] = self.vehicle_statuses[i][:]
                    new_statuses[i][1] = new_loc
                    states.append(rushhour(direction, self.gval + 1, self, self.board_properties, new_statuses))
        return states

    def hashable_state(self):
#IMPLEMENT
        '''Return a data item that can be used as a dictionary key to UNIQUELY represent the state.'''
        op = self.get_board_properties()
        for vehicle in self.get_vehicle_statuses():
            op += (vehicle[0],) + vehicle[1]
        return op
    
    def print_state(self):
        #DO NOT CHANGE THIS FUNCTION---it will be used in auto marking
        #and in generating sample trace output.
        #Note that if you implement the "get" routines
        #(rushhour.get_vehicle_statuses() and rushhour.get_board_size())
        #properly, this function should work irrespective of how you represent
        #your state.

        if self.parent:
            print("Action= \"{}\", S{}, g-value = {}, (From S{})".format(self.action, self.index, self.gval, self.parent.index))
        else:
            print("Action= \"{}\", S{}, g-value = {}, (Initial State)".format(self.action, self.index, self.gval))

        print("Vehicle Statuses")
        for vs in sorted(self.get_vehicle_statuses()):
            print("    {} is at ({}, {})".format(vs[0], vs[1][0], vs[1][1]), end="")
        board = get_board(self.get_vehicle_statuses(), self.get_board_properties())
        print('\n')
        print('\n'.join([''.join(board[i]) for i in range(len(board))]))

#Data accessor routines.

    def get_vehicle_statuses(self):
#IMPLEMENT
        '''Return list containing the status of each vehicle
           This list has to be in the format: [vs_1, vs_2, ..., vs_k]
           with one status list for each vehicle in the state.
           Each vehicle status item vs_i is itself a list in the format:
                 [<name>, <loc>, <length>, <is_horizontal>, <is_goal>]
           Where <name> is the name of the robot (a string)
                 <loc> is a location (a pair (x,y)) indicating the front of the vehicle,
                       i.e., its length is counted in the positive x- or y-direction
                       from this point
                 <length> is the length of that vehicle
                 <is_horizontal> is true iff the vehicle is oriented horizontally
                 <is_goal> is true iff the vehicle is a goal vehicle
        '''
        return self.vehicle_statuses

    def get_board_properties(self):
#IMPLEMENT
        '''Return (board_size, goal_entrance, goal_direction)
           where board_size = (m, n) is the dimensions of the board (m rows, n columns)
                 goal_entrance = (x, y) is the location of the goal
                 goal_direction is one of 'N', 'E', 'S' or 'W' indicating
                                the orientation of the goal
        '''
        return self.board_properties
    
    def occupied(self):
        (m, n) = self.board_properties[0]
        self.board = []
        for x in range(n):
            self.board.append([])
            for y in range(m):
                self.board[x].append(0)
                
        for vehicle in self.vehicle_statuses:
            for i in range(vehicle[2]): #length
                self.board[(vehicle[1][0] + vehicle[3]*i)%n][(vehicle[1][1] + (1-vehicle[3])*i)%m] = 1
        
#############################################
# heuristics                                #
#############################################


def heur_zero(state):
    '''Zero Heuristic use to make A* search perform uniform cost search'''
    return 0


def heur_min_moves(state):
#IMPLEMENT
    '''rushhour heuristic'''
    #We want an admissible heuristic. Getting to the goal requires
    #one move for each tile of distance.
    #Since the board wraps around, there are two different
    #directions that lead to the goal.
    #NOTE that we want an estimate of the number of ADDITIONAL
    #     moves required from our current state
    #1. Proceeding in the first direction, let MOVES1 =
    #   number of moves required to get to the goal if it were unobstructed
    #2. Proceeding in the second direction, let MOVES2 =
    #   number of moves required to get to the goal if it were unobstructed
    #
    #Our heuristic value is the minimum of MOVES1 and MOVES2 over all goal vehicles.
    #You should implement this heuristic function exactly, even if it is
    #tempting to improve it.
    board = state.get_board_properties()
    (m, n) = board[0]
    loc = board[1]
    direction = board[2]
    min_h = m + n + 1
    
    for vehicle in state.get_vehicle_statuses():
        goal_x = loc[0]
        goal_y = loc[1]
        x = vehicle[1][0]
        y = vehicle[1][1]
        length = vehicle[2]
        is_hori = vehicle[3]
        if not vehicle[4]:
            continue
        if ((is_hori or x != goal_x) and direction in ['N','S']) or ((not is_hori or y != goal_y) and direction in ['W','E']):
            heuristic = m + n + 1
        elif direction == 'N':
            if goal_y <= y:
                heuristic = y - goal_y
            else:
                heuristic = m - goal_y + y
                
        elif direction == 'S':
            if goal_y >= (y + length - 1) % m:
                heuristic = goal_y - (y + length - 1) % m
            else:
                heuristic = m - (y + length - 1) % m + goal_y
                
        elif direction == 'W':
            if goal_x <= x:
                heuristic = x - goal_x
            else:
                heuristic = n + x - goal_x
                
        elif direction == 'E':
            if goal_x >= (x + length - 1) % n:
                heuristic = goal_x - (x + length - 1) % n
            else:
                heuristic = n - (x + length - 1) % n + goal_x
        else:
            print('Invalid goal_direction')
            raise Error
        if min_h > heuristic:
            min_h = heuristic
    return min_h

def rushhour_goal_fn(state):
#IMPLEMENT
    '''Have we reached a goal state'''
    return heur_min_moves(state) == 0


def make_init_state(board_size, vehicle_list, goal_entrance, goal_direction):
#IMPLEMENT
    '''Input the following items which specify a state and return a rushhour object
       representing this initial state.
         The state's its g-value is zero
         The state's parent is None
         The state's action is the dummy action "START"
       board_size = (m, n)
          m is the number of rows in the board
          n is the number of columns in the board
       vehicle_list = [v1, v2, ..., vk]
          a list of vehicles. Each vehicle vi is itself a list
          vi = [vehicle_name, (x, y), length, is_horizontal, is_goal] where
              vehicle_name is the name of the vehicle (string)
              (x,y) is the location of that vehicle (int, int)
              length is the length of that vehicle (int)
              is_horizontal is whether the vehicle is horizontal (Boolean)
              is_goal is whether the vehicle is a goal vehicle (Boolean)
      goal_entrance is the coordinates of the entrance tile to the goal and
      goal_direction is the orientation of the goal ('N', 'E', 'S', 'W')

   NOTE: for simplicity you may assume that
         (a) no vehicle name is repeated
         (b) all locations are integer pairs (x,y) where 0<=x<=n-1 and 0<=y<=m-1
         (c) vehicle lengths are positive integers
    '''
    #__init__(self, action, gval, parent, board_properties, vehicle_statuses):
    board_properties = (board_size, goal_entrance, goal_direction)

    return rushhour('start', 0, None, board_properties, vehicle_list)

########################################################
#   Functions provided so that you can more easily     #
#   Test your implementation                           #
########################################################


def get_board(vehicle_statuses, board_properties):
    #DO NOT CHANGE THIS FUNCTION---it will be used in auto marking
    #and in generating sample trace output.
    #Note that if you implement the "get" routines
    #(rushhour.get_vehicle_statuses() and rushhour.get_board_size())
    #properly, this function should work irrespective of how you represent
    #your state.
    (m, n) = board_properties[0]
    board = [list(['.'] * n) for i in range(m)]
    for vs in vehicle_statuses:
        for i in range(vs[2]):  # vehicle length
            if vs[3]:
                # vehicle is horizontal
                board[vs[1][1]][(vs[1][0] + i) % n] = vs[0][0]
                # represent vehicle as first character of its name
            else:
                # vehicle is vertical
                board[(vs[1][1] + i) % m][vs[1][0]] = vs[0][0]
                # represent vehicle as first character of its name
    # print goal
    board[board_properties[1][1]][board_properties[1][0]] = board_properties[2]
    return board


def make_rand_init_state(nvehicles, board_size):
    '''Generate a random initial state containing
       nvehicles = number of vehicles
       board_size = (m,n) size of board
       Warning: may take a long time if the vehicles nearly
       fill the entire board. May run forever if finding
       a configuration is infeasible. Also will not work any
       vehicle name starts with a period.

       You may want to expand this function to create test cases.
    '''

    (m, n) = board_size
    vehicle_list = []
    board_properties = [board_size, None, None]
    for i in range(nvehicles):
        if i == 0:
            # make the goal vehicle and goal
            x = randint(0, n - 1)
            y = randint(0, m - 1)
            is_horizontal = True if randint(0, 1) else False
            vehicle_list.append(['gv', (x, y), 2, is_horizontal, True])
            if is_horizontal:
                board_properties[1] = ((x + n // 2 + 1) % n, y)
                board_properties[2] = 'W' if randint(0, 1) else 'E'
            else:
                board_properties[1] = (x, (y + m // 2 + 1) % m)
                board_properties[2] = 'N' if randint(0, 1) else 'S'
        else:
            board = get_board(vehicle_list, board_properties)
            conflict = True
            while conflict:
                x = randint(0, n - 1)
                y = randint(0, m - 1)
                is_horizontal = True if randint(0, 1) else False
                length = randint(2, 3)
                conflict = False
                for j in range(length):  # vehicle length
                    if is_horizontal:
                        if board[y][(x + j) % n] != '.':
                            conflict = True
                            break
                    else:
                        if board[(y + j) % m][x] != '.':
                            conflict = True
                            break
            vehicle_list.append([str(i), (x, y), length, is_horizontal, False])

    return make_init_state(board_size, vehicle_list, board_properties[1], board_properties[2])


def test(nvehicles, board_size):
    s0 = make_rand_init_state(nvehicles, board_size)
    se = SearchEngine('astar', 'full')
    #se.trace_on(2)
    final = se.search(s0, rushhour_goal_fn, heur_min_moves)
    